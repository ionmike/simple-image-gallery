**Simple Image Gallery**

Built on Django 1.9

Packages: Pillow, sorl-thumbnail

```
#!python
python manage.py makemigrations
python manage.py migrate

```

Admin panel: admin (admin@imagegallery.com) / mariohead