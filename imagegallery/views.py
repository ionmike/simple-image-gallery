from django.shortcuts import render, redirect

from .models import Image
from .forms import ImageUploadForm


def index(request):
    images = Image.objects.all()
    context = {'images': images}
    return render(request, 'imagegallery/index.html', context)


def upload(request):
    if request.method == "POST":
        # do the upload and redirect
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.cleaned_data['image']

            image_types = ['image/png', 'image/jpg', 'image/jpeg', 'image/pjpeg',
                           'image/gif']

            if file.content_type not in image_types:
                return render(request, 'imagegallery/upload.html', {'form': form})

            m = Image()
            m.image = form.cleaned_data['image']
            m.comment = form.cleaned_data['comment']
            m.save()
            return redirect('index')
        else:
            return render(request, 'imagegallery/upload.html', {'form': form})
    else:
        return render(request, 'imagegallery/upload.html')
