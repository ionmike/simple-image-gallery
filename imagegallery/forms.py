from django import forms


class ImageUploadForm(forms.Form):
    image = forms.ImageField(label='Select a file')
    comment = forms.CharField(max_length=300)
