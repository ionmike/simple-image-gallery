from django.contrib import admin

from .models import Image


# class ImageAdmin(admin.ModelAdmin):
#     list_display = ('image_display', 'comment')
#
#     def image_display(self, obj):
#         return '<img src="/images/{0}">'.format(self.image.url)
#
#     image_display.short_description = 'Thumbnail'
#     image_display.allow_tags = True

admin.site.register(Image)  # , ImageAdmin
