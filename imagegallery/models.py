from django.db import models
from sorl.thumbnail import ImageField


class Image(models.Model):
    image = ImageField(upload_to='images/%Y/%m/%d')
    comment = models.CharField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)
